#!/usr/bin/env python
from bottle import default_app

from desafio_finix.__main__ import main


if __name__ == '__main__':
    main()
else:
    app = application = default_app()