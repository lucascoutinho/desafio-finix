from unittest import TestCase
from hashlib import sha1

from desafio_finix.admin.auth import generate_token, roolback_auth
from desafio_finix.querysets import create_user


class TestAuth(TestCase):

    def test_generate_and_roolback(self):
        user = create_user({'name': 'auth_user', 'email': 'user@email.com',
                            'password': 'auth_pwd'})
        token = generate_token(user)
        user_auth = roolback_auth(token)
        self.assertEqual(user_auth.name, 'auth_user')
        self.assertEqual(user_auth.email, 'user@email.com')
        password_sha1 = sha1('auth_pwd'.encode('utf-8')).hexdigest()
        self.assertEqual(user_auth.password, password_sha1)
