from unittest import TestCase, mock

from bottle import FormsDict

from desafio_finix import neighbourhood_after_save
from desafio_finix.admin.base import generic_add, register, ACTIVE_SERVICES
from desafio_finix.admin.forms import NeighbourhoodForm, PropertyForm
from desafio_finix.models import Neighbourhood, Property
from desafio_finix.querysets import create_city, create_neighbourhood, \
    get_list_neighbourhood

MODULE = 'desafio_finix.admin.base.'
MODULE_DECORATOR = 'desafio_finix.admin.decorators.'
MODULE_REGISTER = 'desafio_finix.admin.register.'


def property_after_save(*args):
    form, post = args
    form.neighbourhood.choices = get_list_neighbourhood()
    return form, post


class TestRegisterNeighbourhood(TestCase):

    def setUp(self):
        self.patch_request = mock.patch(MODULE + 'request')
        self.mock_request = self.patch_request.start()
        self.mock_request.forms.get.return_value = None

        self.patch_response = mock.patch(MODULE + 'response')
        self.mock_response = self.patch_response.start()

        self.patch_request_dec = mock.patch(MODULE_DECORATOR + 'request')
        self.mock_request_dec = self.patch_request_dec.start()

        self.patch_roolback_dec = mock.patch(MODULE_DECORATOR + 'roolback_auth')
        self.mock_roolback_dec = self.patch_roolback_dec.start()
        self.mock_roolback_dec.return_value = True

        self.patch_redirect = mock.patch(MODULE + 'redirect')
        self.mock_redirect = self.patch_redirect.start()
        hooks = {'after_save': neighbourhood_after_save}
        self.service = register('neighbourhood', 'Bairro', Neighbourhood,
                                NeighbourhoodForm, hooks=hooks)

    def test_generic_add_empty(self):
        self.mock_request.POST = FormsDict()
        generic_add(ACTIVE_SERVICES['neighbourhood'])
        self.mock_response.flash.assert_called_once_with('Preencha o formulário corretamente')

    def test_generic_add_with_valid_fields(self):
        create_city({'name': 'Rio de Janeiro'})
        self.mock_request.POST = FormsDict({'name': 'Rio de Janeiro', 'city': '1'})
        generic_add(ACTIVE_SERVICES['neighbourhood'])
        self.mock_response.flash.assert_called_once_with('Registro gravado')
        self.mock_redirect.assert_called_once_with('/admin/neighbourhood/list')


class TestRegisterProperty(TestCase):

    def setUp(self):
        self.patch_request = mock.patch(MODULE + 'request')
        self.mock_request = self.patch_request.start()
        self.mock_request.forms.get.return_value = None

        self.patch_response = mock.patch(MODULE + 'response')
        self.mock_response = self.patch_response.start()

        self.patch_request_dec = mock.patch(MODULE_DECORATOR + 'request')
        self.mock_request_dec = self.patch_request_dec.start()

        self.patch_roolback_dec = mock.patch(MODULE_DECORATOR + 'roolback_auth')
        self.mock_roolback_dec = self.patch_roolback_dec.start()
        self.mock_roolback_dec.return_value = True

        self.patch_redirect = mock.patch(MODULE + 'redirect')
        self.mock_redirect = self.patch_redirect.start()
        hooks = {'after_save': property_after_save}
        self.service = register('property', 'Imóvel', Property, PropertyForm,
                                hooks=hooks)

    def test_generic_add_empty(self):
        self.mock_request.POST = FormsDict()
        generic_add(ACTIVE_SERVICES['property'])
        self.mock_response.flash.assert_called_once_with('Preencha o formulário corretamente')

    def test_generic_add_with_valid_fields(self):
        neighbourhood = create_neighbourhood({'city': 1, 'name': 'Centro'})
        self.mock_request.POST = FormsDict({'description': 'test_case',
                                            'value': 100, 'taxes': 'free',
                                            'rooms': 2, 'square_area': 90,
                                            'neighbourhood': neighbourhood.id,
                                            'photos': []})
        generic_add(ACTIVE_SERVICES['property'])
        self.mock_response.flash.assert_called_once_with('Registro gravado')
        self.mock_redirect.assert_called_once_with('/admin/property/list')

