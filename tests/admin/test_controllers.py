from unittest import TestCase, mock

from bottle import FormsDict

from desafio_finix.admin.controllers import post_admin_login, admin_logout
from desafio_finix.querysets import create_user

MODULE = 'desafio_finix.admin.controllers.'


class TestControllers(TestCase):

    def setUp(self):
        self.patch_request = mock.patch(MODULE + 'request')
        self.mock_request = self.patch_request.start()

        self.patch_response = mock.patch(MODULE + 'response')
        self.mock_response = self.patch_response.start()

        self.patch_redirect = mock.patch(MODULE + 'redirect')
        self.mock_redirect = self.patch_redirect.start()

    def tearDown(self):
        self.patch_request.stop()
        self.patch_response.stop()
        self.patch_redirect.stop()

    def test_post_admin_login_with_empty_form(self):
        self.mock_request.POST = FormsDict()
        post_admin_login()
        self.assertEqual(self.mock_response.flash.call_args,
                         mock.call('Campos inválidos'))
        self.mock_redirect.assert_called_once_with('/admin/login')

    def test_post_admin_login_with_invalid_user(self):
        self.mock_request.POST = FormsDict({'name': 'user', 'password':
                                            'password'})
        post_admin_login()
        self.assertEqual(self.mock_response.flash.call_args,
                         mock.call('Senha ou login inválidos'))
        self.mock_redirect.assert_called_once_with('/admin/login')

    def test_post_admin_login_with_valid_user(self):
        create_user({'name': 'valid_login', 'email': 'my@email.com',
                     'password': '123123'})
        self.mock_request.POST = FormsDict({'name': 'valid_login', 'password':
                                            '123123'})
        post_admin_login()
        self.assertIsNone(self.mock_response.flash.call_args)
        self.mock_redirect.assert_called_once_with('/admin/property/list')

    def test_logout(self):
        admin_logout()
        self.assertEqual(self.mock_response.delete_cookie.call_args,
                         mock.call('token'))
        self.mock_redirect.assert_called_once_with('/admin/login')

