from unittest import TestCase, mock

from desafio_finix import generate_token
from desafio_finix.admin.decorators import login_required
from desafio_finix.querysets import create_user

MODULE = 'desafio_finix.admin.decorators.'


class TestDecorators(TestCase):

    def setUp(self):
        self.patch_request = mock.patch(MODULE + 'request')
        self.mock_request = self.patch_request.start()

        self.patch_response = mock.patch(MODULE + 'response')
        self.mock_response = self.patch_response.start()

        self.patch_redirect = mock.patch(MODULE + 'redirect')
        self.mock_redirect = self.patch_redirect.start()

    def tearDown(self):
        self.mock_request.stop()
        self.mock_response.stop()
        self.mock_redirect.stop()

    def test_login_failed(self):

        @login_required
        def controller():
            return True

        self.mock_request.get_cookie.return_value = False
        self.assertEqual(controller(), self.mock_redirect())
        self.mock_response.flash.call_args('Login do usuário obrigatório')
        self.mock_redirect.call_args('/admin/login')

    def test_login_success(self):

        @login_required
        def controller():
            return 'logged'

        user = create_user({'name': 'auth_user_decorator', 'email':
                            'user@email.com', 'password': 'decorated_auth_pwd'})
        token = generate_token(user)
        self.mock_request.get_cookie.return_value = token
        self.assertEqual(controller(), 'logged')

