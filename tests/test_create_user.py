from hashlib import sha1
from unittest import TestCase

from pony.orm import db_session, get

from desafio_finix.create_user import create_user
from desafio_finix.models import User


class TestCreateUser(TestCase):

    @db_session
    def test_create_user(self):
        user = {
            'name': 'user',
            'email': 'user@email.com',
            'password': 'user_password'
        }
        create_user(user)
        user = get(user for user in User if user.name == 'user')
        self.assertEqual(user.name, 'user')
        self.assertEqual(user.email, 'user@email.com')
        sha1_password = sha1('user_password'.encode('utf-8')).hexdigest()
        self.assertEqual(user.password, sha1_password)
