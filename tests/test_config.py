from unittest import TestCase, mock

import desafio_finix.config as config

MODULE = 'desafio_finix.config.'


class TestConfig(TestCase):

    def setUp(self):
        mock_db = mock.patch(MODULE + 'Database')
        self.mock_db = mock_db.start().return_value

    def tearDown(self):
        self.mock_db.stop()

    def test_environ_test_mode(self):
        self.assertTrue(config.TEST)

    def test_sqlite_memory_on_production_mode(self):
        config.TEST = False
        config.get_db()
        self.assertEqual(self.mock_db.bind.call_args,
                         mock.call('sqlite', mock.ANY, create_db=True))

    def test_sqlite_memory_on_test_mode(self):
        config.TEST = True
        config.get_db()
        self.mock_db.bind.assert_called_once_with('sqlite', ':memory:')



