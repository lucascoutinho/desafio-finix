from unittest import TestCase

from desafio_finix.models import get_entity_fields, User


class TestModels(TestCase):

    def test_get_entity_fields_for_user(self):
        self.assertEqual(get_entity_fields(User), ['id', 'name',
                                                   'email', 'password',
                                                   'last_login'])
