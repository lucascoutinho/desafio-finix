from unittest import TestCase
from datetime import datetime

from pony.orm import db_session

from desafio_finix.querysets import (
    user_login, create_user, update_user_last_login
)


class TestQuerysets(TestCase):

    def test_user_login(self):
        create_user({'name': 'user_ok', 'email': 'user@email.com',
                     'password': 'password_ok'})
        self.assertTrue(user_login('user_ok', 'password_ok'))

    def test_user_login_fail(self):
        self.assertFalse(user_login('user_name', 'password'))

    @db_session
    def test_update_user_last_login(self):
        user = {
            'name': 'user_name',
            'email': 'user_mail',
            'password': 'user_pwd'
        }
        db_user = create_user(user=user)
        self.assertIsNone(db_user.last_login)
        db_user = update_user_last_login(db_user)
        self.assertIsInstance(db_user.last_login, datetime)

