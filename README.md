# Desafio Finix

## Requisitos: 
* Linux
* Docker
* docker-compose com suporte a versão 2 da syntax


## Instruções para rodar o projeto

**Importante criar um novo usuário para o admin antes**

_Criação do usuário_
```
make user 
```


_Executar_
```
make run 
```

_Rodar testes_
```
make test 
```

_Criar ambiente virtual_
(Caso queira rodar na própria máquina)

```
make venv 
```

## Roda do admin

```
http://localhost:8080/admin
```
