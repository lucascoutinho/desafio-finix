% rebase base title='Home'


<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="navbar-header">
        <a class="navbar-brand" href="#">Desafio Finix</a>
    </div>
</nav>
<div class="container">
    <ul class="row">
    %for property in properties:
        <li class="col-lg-3 col-md-4 col-xs-6 thumb">
            <h3>{{property.description}}</h3>
            %for photo in property.photos:
                <img src="{{photo.path}}" class="img-responsive" />
                %break
            %end
            <p>Valor: <strong>{{property.value}}</strong></p>
            <p>Dormitórios: {{property.rooms}}</p>
            <p>Bairro: {{property.neighbourhood.name}}</p>
        </li>
    %end
    </ul>
</div>
