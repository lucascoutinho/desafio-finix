<!doctype html>
<html>
<head>
    <title>Finix- {{title or 'Home'}}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/bootstrap-theme.css">
    <link rel="stylesheet" href="/assets/css/style.css">
</head>
<body>
{{!base}}
</body>
</html>
