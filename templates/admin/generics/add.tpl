% rebase admin/base title='Listagem'

<form class="" action="/admin/{{ routing_key}}/add" method="POST" enctype="multipart/form-data">
    %if message:
    <p class="alert-error">{{ message }}</p>
    %end
    %for field in form:
        <div class="form-group row">
            % if not field.flags.hidden:
                {{field.label(class_="col-sm-2 col-form-label")}}
            %end
            <div class="col-sm-10">
                % if field.flags.required:
                <div class="required">
                    * obrigatório
                </div>
                %end
                {{field(class_="form-control")}}
            </div>
        </div>
    %end
    %if include_tpl:
        % include(include_tpl)
    %end
    <div class="form-group row">
      <div class="offset-sm-2 col-sm-10">
        <button type="submit" class="btn btn-primary">Salvar</button>
      </div>
    </div>
</form>
