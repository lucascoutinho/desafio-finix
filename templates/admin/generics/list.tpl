% rebase admin/base title='Listagem'

<a class="btn btn-success add-btn" href="/admin/{{ routing_key }}/add">Adicionar</a>

%if result_set:
    <table class="table table-striped">
        <thead>
            <tr>
            %for column in columns:
                <th>{{column}}</th>
            %end
                <th>Editar</th>
                <th>Excluir</th>
            <tr>
        </thread>

        <tbody>
        %for obj in result_set:
            <tr>
            %for column in columns:
                <td class="row">{{ getattr(obj, column) }}</td>
            %end
                <td class="row">
                    <a class="edit_obj" href="/admin/{{ routing_key }}/add/{{obj.id}}">Editar</a>
                </td>
                <td class="row">
                    <a class="delete_obj" onclick="return confirm('Deseja continuar?');" href="/admin/{{ routing_key }}/delete/{{obj.id}}">Excluir</a>
                </td>
            </tr>
        %end
        </tbody>
    </table>
%end