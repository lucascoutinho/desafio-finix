%if record:

    %if record.photos:
        %for photo in record.photos:
            <li><img src="{{photo.path}}.thumbnail" width="150" /></li>
        %end
    %end

%end