<!doctype html>
<html>
<head>
	<title>Admin</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<link rel="stylesheet" href="/assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="/assets/css/bootstrap-theme.css">
	<link rel="stylesheet" href="/assets/css/admin/login.css">
</head>
<body>
	<div class="container">
		<form class="form-signin" action="/admin/login" method="POST">
			%if message:
                <p class="alert-error">{{ message }}</p>
			%end
			<h2 class="form-signin-heading">Desafio Finix</h2>
			%for field in form:
                {{field.label}}
                {{field}}
			%end
			<button class="btn btn-lg btn-primary btn-block" type="submit">Login
			</button>
		</form>
	</div>
</body>
</html>
