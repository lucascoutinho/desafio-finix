<!doctype html>
<html>
<head>
    <title>Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/bootstrap-theme.css">
    <link rel="stylesheet" href="/assets/css/admin/style.css">
</head>
<body>
% include('shared/header.tpl')
<div class="container">
    {{!base}}
</div>
% include('shared/footer.tpl')
</body>
</html>
