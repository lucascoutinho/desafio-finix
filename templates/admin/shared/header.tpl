<nav>
    <ul>
        %for service, item in menu:
            <li><a href="/admin/{{service}}/list">{{item}}</a></li>
        %end
            <li><a href="/admin/logout" class="logout">Logout</a></li>
    </ul>
</nav>