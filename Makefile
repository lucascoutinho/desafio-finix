.SILENT:

venv:
	virtualenv venv --python=python3.5

user:
	python -m desafio_finix.create_user

test:
	test=1 python -m unittest -v

build:
	docker-compose build

run:
	docker-compose up -d

stop:
	docker-compose stop

