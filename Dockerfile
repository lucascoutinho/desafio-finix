FROM python:3.5.2-alpine

ENV TERM=xterm TIMEZONE=America/Sao_Paulo
WORKDIR /opt/app
ADD . /opt/app

ENV LIBRARY_PATH=/lib:/usr/lib
RUN apk add --update --no-cache tzdata ca-certificates musl-dev python3-dev py-pip jpeg-dev zlib-dev gcc \
    && pip3 install -U pip \
    && echo ${TIMEZONE} > /etc/timezone \
    && ln -sf /usr/share/zoneinfo/${TIMEZONE} /etc/localtime \
    && pip install -U -r requirements.txt
