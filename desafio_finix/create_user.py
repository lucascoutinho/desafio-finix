from desafio_finix.querysets import create_user


def input_user():
    print('{:-^50}'.format('  Input user  '))
    name = input('Name: ')
    email = input('Email: ')
    password = input('Password: ')
    return {'name': name, 'email': email, 'password': password}


if __name__ == '__main__':
    create_user(input_user())
