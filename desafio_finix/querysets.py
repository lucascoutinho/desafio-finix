from typing import Dict
from hashlib import sha1
from datetime import datetime

from pony.orm import db_session, commit, select

from desafio_finix.models import User, City, Neighbourhood, Property


@db_session
def user_login(name: str, password: str):
    sha1_password = str(sha1(password.encode('utf-8')).hexdigest())
    return User.get(name=name, password=sha1_password)


@db_session
def create_user(user: Dict):
    created_user = User(**user)
    commit()
    return created_user


@db_session
def update_user_last_login(user: User):
    user.last_login = datetime.now()
    return user


@db_session
def get_list_cities():
    return [(city.id, city.name) for city in City.select()]


@db_session
def get_list_neighbourhood():
    return [(neigh.id, neigh.name + ' - ' + neigh.city.name) for neigh in
            Neighbourhood.select()]


@db_session
def create_city(city):
    created_city = City(**city)
    commit()
    return created_city


@db_session
def create_neighbourhood(neighbourhood):
    created_neighbourhood = Neighbourhood(**neighbourhood)
    commit()
    return created_neighbourhood


@db_session
def get_properties(**kwargs):
    properties = select(prop for prop in Property).filter(**kwargs)
    return [prop for prop in properties]
