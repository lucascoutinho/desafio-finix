from bottle import route, request, template
from pony.orm import db_session

from desafio_finix.querysets import get_properties


@route('/')
@db_session
def index():
    properties = get_properties(**request.query)
    context = {'properties': properties}
    return template('home', context)




