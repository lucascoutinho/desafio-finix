import os

from bottle import request

from desafio_finix.models import Property, PropertyPictures, City, Neighbourhood
from desafio_finix.admin.forms import PropertyForm, CityForm, NeighbourhoodForm
from desafio_finix.querysets import get_list_cities, get_list_neighbourhood
from desafio_finix.consts import UPLOAD_PATH
from desafio_finix.admin.base import register
from desafio_finix.admin.image import resize


def neighbourhood_after_save(*args):
    form, post = args
    form.city.choices = get_list_cities()
    return form, post


def property_after_save(*args):
    form, post = args
    form.neighbourhood.choices = get_list_neighbourhood()
    photos = request.files.getall('photos')
    post_photos = []
    if not os.path.exists(UPLOAD_PATH):
        os.mkdir(UPLOAD_PATH)
    for photo in photos:
        filepath = UPLOAD_PATH + photo.filename
        if os.path.exists(filepath):
            os.remove(filepath)
        photo.save(UPLOAD_PATH)
        resize(filepath)
        relative_path = '/' + '/'.join(filepath.split('/')[-3:])
        post_photos.append({'path': relative_path})
    if photos:
        post.update({'photos': [PropertyPictures(**post_photo)
                                for post_photo in post_photos]})
    return form, post


register('city', 'Cidades', City, CityForm)

register('neighbourhood', 'Bairros', Neighbourhood, NeighbourhoodForm,
         hooks={'after_save': neighbourhood_after_save})

register('property', 'Imóveis', Property, PropertyForm,
         hooks={'after_save': property_after_save},
         options={'include_tpl': 'admin/property/pictures.tpl'})
