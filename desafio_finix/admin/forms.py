from wtforms import (
    Form, FileField, TextAreaField, StringField, PasswordField, SelectField,
    validators, HiddenField
)
from wtforms import ValidationError

from desafio_finix.querysets import get_list_cities, get_list_neighbourhood


class UserLogin(Form):

    name = StringField(label='Usuário', validators=[validators.input_required()])
    password = PasswordField(label='Senha', validators=[
                             validators.input_required()])


class FormBaseId(Form):

    id = HiddenField()


class PropertyForm(FormBaseId):

    description = TextAreaField(label='Descrição', validators=
                                [validators.input_required()])
    value = StringField(label='Valor', render_kw={'placeholder': 'Valor do imóvel'})
    taxes = StringField(label='Impostos', render_kw={'placeholder': 'Ex: iptu'})
    rooms = StringField(label='Quartos')
    square_area = StringField(label='Área quadrada')
    neighbourhood = SelectField(label='Bairro', coerce=int,
                                choices=get_list_neighbourhood())

    photos = FileField(label='Selecione as fotos (segure o control para '
                            'selecionar mais de uma)', render_kw={'multiple': True})

    def validate_value(self, field):
        try:
            float(field.data)
        except:
            raise ValidationError('O valor precisa ser numerico')


class CityForm(FormBaseId):

    name = StringField(label='Nome', validators=[validators.input_required()])


class NeighbourhoodForm(FormBaseId):

    name = StringField(label='Nome', validators=[validators.input_required()])
    city = SelectField(label='Cidade', coerce=int, choices=get_list_cities())
