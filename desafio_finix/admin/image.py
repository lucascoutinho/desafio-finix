from PIL import Image

from resizeimage import resizeimage


def resize(filepath, size=(150, 150)):
    name = 'thumb_' + filepath.split('/')[-1]
    thumbpath = '/'.join(filepath.split('/')[:-1]) + name
    with open(filepath, 'r+b') as _file:
        with Image.open(_file) as image:
            cover = resizeimage.resize_cover(image, size)
            cover.save(thumbpath, image.format)
