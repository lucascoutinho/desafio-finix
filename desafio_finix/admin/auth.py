import base64

from pony.orm import db_session

from desafio_finix.models import User


def generate_token(user: User) -> bytes:
    return b'' + base64.b64encode(user.name.encode('ascii')) + b','  b'' + base64.b64encode(
           user.password.encode('ascii'))


@db_session
def roolback_auth(token: bytes) -> User:
    name, password = token.decode('ascii').split(',')
    name = base64.b64decode(name).decode('ascii')
    password = base64.b64decode(password).decode('ascii')
    return User.get(name=name, password=password)
