from bottle import template, get, post, request, response, redirect

from desafio_finix.admin.auth import generate_token
from desafio_finix.admin.forms import UserLogin
from desafio_finix.querysets import user_login
from desafio_finix.consts import COOKIE_SECRET


@get('/admin/login')
def get_admin_login():
    form = UserLogin()
    context = {'form': form, 'message': request.message}
    return template('admin/login', context,
                    template_settings={'noescape': True})


@post('/admin/login')
def post_admin_login():
    form = UserLogin(request.POST)
    if form.validate():
        user = user_login(**request.POST)
        if user:
            response.set_cookie('token', generate_token(user), COOKIE_SECRET)
            return redirect('/admin/property/list')
        else:
            response.flash('Senha ou login inválidos')
    else:
        response.flash('Campos inválidos')
    return redirect('/admin/login')


@get('/admin/logout')
def admin_logout():
    response.delete_cookie('token')
    redirect('/admin/login')


