from functools import wraps

from bottle import request, response, redirect

from desafio_finix.consts import COOKIE_SECRET
from desafio_finix.admin.auth import roolback_auth


def login_required(func):

    @wraps(func)
    def wrapper(*args, **kwargs):
        response.message = None
        token = request.get_cookie('token', False, COOKIE_SECRET)
        if token:
            user = roolback_auth(token)
            if user:
                return func(*args, **kwargs)
        response.flash('Login do usuário obrigatório')
        return redirect('/admin/login')

    return wrapper
