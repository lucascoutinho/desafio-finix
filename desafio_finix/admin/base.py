from pony.orm import db_session, commit, delete
from bottle import (
    install, route, template as render_tpl, redirect, request, response,
    FormsDict
)
from bottle_utils import flash

from desafio_finix.admin.decorators import login_required
from desafio_finix.consts import ADMIN_PER_PAGE

__all__ = ['generic_list', 'generic_add', 'generic_delete',
           'install_flash_plugin']

ACTIVE_SERVICES = {}


class BaseService:

    def __init__(self, routing_key, title, model, form, **kwargs):
        self.routing_key = routing_key
        self.title = title
        self.model = model
        self.form = form
        self.hooks = kwargs.get('hooks', {})
        self.options = kwargs.get('options', {})

    def __getattr__(self, item):
        def method(*args):
            if self.hooks:
                if item in self.hooks.keys():
                    self.hooks[item](*args)
            if args:
                return args
        return method


def install_flash_plugin():
    install(flash.message_plugin)


# TODO: find a better way to use bottle app instance instead of this decorator
def inject_service(func):

    def wrapper(*args, **kwargs):
        if kwargs.get('service'):
            kwargs['service'] = ACTIVE_SERVICES.get(kwargs['service'])
        return func(*args, **kwargs)

    return wrapper


def register(routing_key, title, model, form, **kwargs):
    ACTIVE_SERVICES[routing_key] = BaseService(routing_key, title, model, form,
                                               **kwargs)


# app.router.add_filter('base_service', service_filter)

# Template middleware
@db_session
def template(service=None, *args, **kwargs):
    context_index = 1
    menu = [(key, value.title) for key, value in ACTIVE_SERVICES.items()]
    try:
        args[context_index].update({'menu': menu})
        if service:
            args[context_index].update({'routing_key': service.routing_key})
    finally:
        return render_tpl(*args, **kwargs)


@route('/admin/<service>/list')
@login_required
@inject_service
def generic_list(service):
    try:
        page = request.GET.pop('page')
    except KeyError:
        page = 1
    with db_session:
        # TODO pagination
        result_set = service.model.select().page(page, ADMIN_PER_PAGE)
        context = {'result_set': result_set, 'columns': service.model._columns_}
    return template(service, 'admin/generics/list', context)


@route('/admin/<service>/add', ['GET', 'POST'])
@route('/admin/<service>/add/<_id>', ['GET', 'POST'])
@login_required
@inject_service
@db_session
def generic_add(service, _id=None):
    record = None
    _id = request.forms.get('id') or _id
    if _id is not None:
        record = service.model.get(id=_id)
        form = service.form(FormsDict(record.to_dict()))
    else:
        form = service.form(request.POST)
    response.message = None
    post = dict(request.POST)
    form, post = service.after_save(form, post)
    if post and form.validate():
        if record is not None:
            record.set(**post)
            commit()
        else:
            record = service.model(**post)
        service.before_save(record, form)
        response.flash('Registro gravado')
        return redirect('/admin/{}/list'.format(service.routing_key))
    elif not form.validate():
        response.flash('Preencha o formulário corretamente')

    context = {'form': form, 'message': request.message, 'record': record,
               'include_tpl': service.options.get('include_tpl')}
    return template(service, 'admin/generics/add', context,
                    template_settings={'noescape': True})


@route('/admin/<service>/delete/<_id>')
@login_required
@inject_service
def generic_delete(service, _id: int):
    with db_session:
        delete(m for m in service.model if m.id == _id)
    return redirect('/admin/{}/list'.format(service.routing_key))

