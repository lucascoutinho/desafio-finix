import os

from .config import RELATIVE_PATH

DEBUG = True
DEBUG_SQL = False
RELOAD_APP = True

ROOT_ASSETS_DIR = os.path.join(RELATIVE_PATH, 'assets')

COOKIE_SECRET = 'kl0190Alkz,<lk20AksjkCasjkzA'

ADMIN_PER_PAGE = 20
UPLOAD_PATH = os.path.join(RELATIVE_PATH, 'assets/uploads/')

