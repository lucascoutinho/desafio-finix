from datetime import datetime
from hashlib import sha1

from pony.orm import (
    Required, Optional, Set, composite_key, sql_debug
)

from desafio_finix.config import get_db
from desafio_finix.consts import DEBUG_SQL

db = get_db()


def get_entity_fields(entity: db.Entity):
    return [attr.name for attr in entity._attrs_]


class User(db.Entity):
    name = Required(str)
    email = Required(str)
    password = Required(str)
    last_login = Optional(datetime)

    composite_key(name, email)

    def before_insert(self):
        self.password = str(sha1(self.password.encode('utf-8')).hexdigest())


class Property(db.Entity):
    description = Required(str)
    value = Required(float)
    taxes = Optional(str)
    rooms = Optional(int, default=0)
    square_area = Optional(int, default=0)
    neighbourhood = Required(lambda: Neighbourhood)
    photos = Set(lambda: PropertyPictures, cascade_delete=True)


class PropertyPictures(db.Entity):
    path = Required(str)
    property = Optional(Property)


class City(db.Entity):
    name = Required(str)
    neighbourhoods = Set(lambda: Neighbourhood)

    def __repr__(self):
        # PonyORM why are you like this?
        city = City.get(id=self.id)
        return '{}'.format(city.name)


class Neighbourhood(db.Entity):
    name = Required(str)
    city = Required(City)
    property = Optional(Property)

    def __repr__(self):
        neighbourhood = Neighbourhood.get(id=self.id)
        return '{} - {}'.format(neighbourhood.name, neighbourhood.city.name)

db.generate_mapping(create_tables=True)
sql_debug(DEBUG_SQL)
