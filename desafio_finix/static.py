from bottle import route, static_file

from desafio_finix.consts import ROOT_ASSETS_DIR


@route('/assets/<filename:path>')
def assets(filename):
    return static_file(filename, root=ROOT_ASSETS_DIR)