import os

import bottle
from pony.orm.core import Database


TEST = os.environ.get('test', '0') == '1'
DIR = os.path.dirname(__file__)
RELATIVE_PATH = os.path.abspath(os.path.join(DIR, '../'))

bottle.TEMPLATE_PATH.insert(0, os.path.join(RELATIVE_PATH, 'templates/'))
bottle.TEMPLATE_PATH.insert(1, os.path.join(RELATIVE_PATH, 'templates/admin'))


def get_db():
    db = Database()
    if TEST is True:
        db.bind('sqlite', ':memory:')
    else:
        database_file = os.path.join(RELATIVE_PATH, 'database.sqlite')
        db.bind('sqlite', database_file, create_db=True)
    return db
